#Briefing & Analyse
Maak een **responsive mobile-first webapplicatie** waarin minimaal 3 datasets, afkomstig uit de dataset-pool van de **Worldbank**, verwerkt zijn.
#Functionele specificaties
* One Page Webapplication 
    * Bookmark routes
    * of een webapplicatie bestaande uit verschillende pagina's
* Preloaders/Loaders
    * Om de app in te laden
    * Tijdens het inladen van JSON(P)
    * Lokale data bestanden
* De meeste inhoud wordt beheerd in data bastanden en dynamisch ingeladen
* Adaptive images, video's en sounds
* Google Maps integratie of gelijkaardig
    * Custom look-and-feel
* GEO-location
    * Toon de locatie van de gebruiker op een Map
    * Hou rekening met deze locatie in andere onderdelen van deze app
* Social Media Bookmarking (Open Graph)
* Animaties via SVG en/of Canvas
* Lokaal caching van data en bronbestanden (cache manifest)
* Gebruiker ervaart een interactief webapplicatie
* Gebruiker kan favoriete data lokaal bewaren
* Gebruiker kan de webapplicatie bookmarken in browser, bureaublad en als native app in het overzicht
* Automation verplicht!
    * Componenten worden via Bower toegevoegd in de components folder van de app folder
    * SASS bestanden worden automatisch omgezet in corresponderende CSS-bestanden
    * CSS-bestanden worden met elkaar verbonden in één bestand en geminified
    * De JS code wordt automatisch nagekeken op syntax fouten
    JS-bestanden worden met elkaar verbonden in één bestand en geminified
    * De dist-folder wordt automatisch aangevuld met bestanden en folders via Grunt of Gulp
    * Screenshots van de verschillende breekpunten worden automatisch uitgevoerd via Phantom, Casper of andere bibliotheken
#Technische specificaties
###Frontend

- Core technologies: HTML5, CSS3 en JavaScript
- Template engine: Handlebars
- Storage: JSON bestanden, localstorage
- Bibliotheken: jQuery, lodash.js, crossroads.js, js-signals, chart.js


###Backend

> Er is geen backend aanwezig in deze webapplicatie.
#Persona's 
![enter image description here](http://users.telenet.be/timvanmeirvenne/img/persona1.PNG "Persona 1")
![enter image description here](http://users.telenet.be/timvanmeirvenne/img/persona2.PNG "Persona 2")
![enter image description here](http://users.telenet.be/timvanmeirvenne/img/persona3.PNG "Persona 3")
#Ideënbord
![enter image description here](http://users.telenet.be/timvanmeirvenne/img/ideaboard.png)
#Sitemap
![enter image description here](http://users.telenet.be/timvanmeirvenne/img/sitemap-01.png "Sitemap")
#Wireframes(x3)
![enter image description here](http://users.telenet.be/timvanmeirvenne/img/wireframes-01.png)
![enter image description here](http://users.telenet.be/timvanmeirvenne/img/wireframes-02.png)
![enter image description here](http://users.telenet.be/timvanmeirvenne/img/wireframes-03.png)

#Style Tile
![enter image description here](http://users.telenet.be/timvanmeirvenne/img/Style_Tile.png)

#Screenshots
![enter image description here](http://users.telenet.be/timvanmeirvenne/img/screenshot1.PNG)
![enter image description here](http://users.telenet.be/timvanmeirvenne/img/screenshot2.PNG)
![enter image description here](http://users.telenet.be/timvanmeirvenne/img/screenshot3.PNG)
![enter image description here](http://users.telenet.be/timvanmeirvenne/img/screenshot4.PNG)

#Tijdsbesteding
|	Datum	|	|	Student	|	|	Domein	|	|	Taak	|	|	Tijd	|
|	:-------------:	|	|	:-------------:	|	|	:---------:	|	|	:-------------:	|	|	:-------------:	|
|	1/10/2015	|	|	Tim Van Meirvenne	|	|	DEV	|	|	Project opzetten	|	|	0,25	|
|	3/10/2015	|	|	Tim Van Meirvenne	|	|	DES	|	|	Ideëenbord	|	|	0,5	|
|	5/10/2015	|	|	Tim Van Meirvenne	|	|	DEV	|	|	Bower	|	|	1,5	|
|	7/10/2015	|	|	Tim Van Meirvenne	|	|	DEV	|	|	Coderen	|	|	1	|
|	9/10/2015	|	|	Tim Van Meirvenne	|	|	DES	|	|	Designen	|	|	0,5	|
|	11/10/2015	|	|	Tim Van Meirvenne	|	|	DEV	|	|	Coderen	|	|	0,25	|
|	13/10/2015	|	|	Tim Van Meirvenne	|	|	DES	|	|	Designen	|	|	1	|
|	15/10/2015	|	|	Tim Van Meirvenne	|	|	DEV	|	|	Coderen	|	|	1,75	|
|	17/10/2015	|	|	Tim Van Meirvenne	|	|	DEV	|	|	Coderen	|	|	2	|
|	19/10/2015	|	|	Tim Van Meirvenne	|	|	DES	|	|	Designen	|	|	3	|
|	21/10/2015	|	|	Tim Van Meirvenne	|	|	DEV	|	|	Coderen	|	|	2	|
|	23/10/2015	|	|	Tim Van Meirvenne	|	|	DES	|	|	Designen	|	|	3	|
|	25/10/2015	|	|	Tim Van Meirvenne	|	|	DEV	|	|	Coderen	|	|	2	|
|	27/10/2015	|	|	Tim Van Meirvenne	|	|	DEV	|	|	Coderen	|	|	1,5	|
|	29/10/2015	|	|	Tim Van Meirvenne	|	|	DES	|	|	Designen	|	|	1	|
|	31/10/2015	|	|	Tim Van Meirvenne	|	|	DEV	|	|	Coderen	|	|	0,75	|
|	2/11/2015	|	|	Tim Van Meirvenne	|	|	DES	|	|	Designen	|	|	1	|
|	4/11/2015	|	|	Tim Van Meirvenne	|	|	DEV	|	|	Coderen	|	|	1	|
|	6/11/2015	|	|	Tim Van Meirvenne	|	|	DEV	|	|	Coderen	|	|	1	|
|	8/11/2015	|	|	Tim Van Meirvenne	|	|	DES	|	|	Designen	|	|	5	|
|	10/11/2015	|	|	Tim Van Meirvenne	|	|	DEV	|	|	Coderen	|	|	3	|
|	12/11/2015	|	|	Tim Van Meirvenne	|	|	DES	|	|	Designen	|	|	2,75	|
|	14/11/2015	|	|	Tim Van Meirvenne	|	|	DEV	|	|	Coderen	|	|	2,2	|
|	16/11/2015	|	|	Tim Van Meirvenne	|	|	DEV	|	|	Coderen	|	|	4,5	|
|	18/11/2015	|	|	Tim Van Meirvenne	|	|	DES	|	|	Designen	|	|	4	|
|	20/11/2015	|	|	Tim Van Meirvenne	|	|	DEV	|	|	Coderen	|	|	5	|
|	22/11/2015	|	|	Tim Van Meirvenne	|	|	DES	|	|	Designen	|	|	2	|
|	24/11/2015	|	|	Tim Van Meirvenne	|	|	DEV	|	|	Coderen	|	|	3	|
|	26/11/2015	|	|	Tim Van Meirvenne	|	|	DEV	|	|	Coderen	|	|	2	|
|	28/11/2015	|	|	Tim Van Meirvenne	|	|	DES	|	|	Designen	|	|	3	|
|	30/11/2015	|	|	Tim Van Meirvenne	|	|	DEV	|	|	Coderen	|	|	2	|
|	2/12/2015	|	|	Tim Van Meirvenne	|	|	DES	|	|	Designen	|	|	2	|
|	4/12/2015	|	|	Tim Van Meirvenne	|	|	DEV	|	|	Coderen	|	|	3	|
|	6/12/2015	|	|	Tim Van Meirvenne	|	|	DEV	|	|	Coderen	|	|	2	|
|	8/12/2015	|	|	Tim Van Meirvenne	|	|	DES	|	|	Designen	|	|	3	|
|	10/12/2015	|	|	Tim Van Meirvenne	|	|	DEV	|	|	Coderen	|	|	5	|
|	12/12/2015	|	|	Tim Van Meirvenne	|	|	DES	|	|	Designen	|	|	8	|
|	14/12/2015	|	|	Tim Van Meirvenne	|	|	DEV	|	|	Coderen	|	|	4	|
|	16/12/2015	|	|	Tim Van Meirvenne	|	|	DEV	|	|	Coderen	|	|	5	|
|	18/12/2015	|	|	Tim Van Meirvenne	|	|	DES	|	|	Designen	|	|	5	|
|	20/12/2015	|	|	Tim Van Meirvenne	|	|	DEV	|	|	Coderen	|	|	3	|
|	22/12/2015	|	|	Tim Van Meirvenne	|	|	DES	|	|	Designen	|	|	2	|			


#Datasets
####Totale Death rate
http://data.worldbank.org/indicator/SP.DYN.CDRT.IN/countries?display=default

#### Death by Disease
http://data.worldbank.org/indicator/SH.DTH.COMM.ZS

#### Death By Injury
http://data.worldbank.org/indicator/SH.DTH.INJR.ZS

#### Death By Non-Communicable Diseases
http://data.worldbank.org/indicator/SH.DTH.NCOM.ZS

#### Life Expectancy (female)
http://data.worldbank.org/indicator/SP.DYN.LE00.FE.IN

#### Life Expectancy (male)
http://data.worldbank.org/indicator/SP.DYN.LE00.MA.IN

#### Population
http://data.worldbank.org/indicator/SP.POP.TOTL
