var data = JSON.parse(localStorage.getItem('data'));
var likes = JSON.parse(localStorage.getItem('favo'));
var map;
var src = 'http://users.telenet.be/timvanmeirvenne/data/countries_world.kml';
var myJSON = {};
var filteredCountries = ['East Asia & Pacific (all income levels)', 'East Asia & Pacific (developing only)', 'Europe & Central Asia (all income levels)', 'Europe & Central Asia (developing only)', 'Fragile and conflict affected situations', 'Heavily indebted poor countries (HIPC)', 'High income', 'Latin America & Caribbean (all income levels)', 'Latin America & Caribbean (developing only)', 'Least developed countries: UN classification', 'Low income', 'Middle East & North Africa (all income levels)', 'High income: OECD', 'High income: nonOECD', 'Low & middle income', 'Lower middle income', 'Middle East & North Africa (developing only)', 'Middle income', 'OECD members', 'Other small states', 'European Union', 'Euro area', 'Central Europe and the Baltics', 'Caribbean small states', 'Central African Republic', 'North America', 'Arab World', 'Small states', 'Solomon Islands', 'South Asia', 'South Sudan', 'Sub-Saharan Africa (all income levels)', 'Sub-Saharan Africa (developing only)', 'Upper middle income', 'World', 'Brunei Darussalam', 'Trinidad and Tobago', 'Timor-Leste'];
var year = 50;
var userLat, userLng;
var locations = {};
var lstCloseEnough = {};
var km;
var countryuser;
var distance;
var difference;
var mySlider;
var crossroads, hasher;

function init() {
  //setup crossroads
  crossroads.addRoute('');
  crossroads.addRoute('/favo');
  crossroads.addRoute('/table');
  crossroads.addRoute('/world');
  crossroads.addRoute('/detail');
  crossroads.addRoute('/lifeextender');
  crossroads.addRoute('/detail/{country}');
  crossroads.routed.add(parseRoute, console); //log all routes
  
  //setup hasher
  function parseHash(newHash) {
    crossroads.parse(newHash);
  }
  
  hasher.initialized.add(parseHash); //parse initial hash
  hasher.changed.add(parseHash); //parse hash changes
  hasher.init(); //start listening for history change
  
  //load data
  //localStorage.clear();
  storeData();
  fillTable(year);
  
}


function storeData() {
  if (localStorage.getItem('favo') == null) {
    var likes = {};
    localStorage.setItem('favo', JSON.stringify(likes));
  }
  if (localStorage.getItem('data') == null) {
    
    $.getJSON('https://api.myjson.com/bins/v8bz', function (data) {
    $.each(data, function (key, val) {
      if (filteredCountries.indexOf(key) == -1 && val[2014] !== null) {
        myJSON[key] = {};
        myJSON[key].DBCD = val[2014];
      }
    });
    
    $.getJSON('https://api.myjson.com/bins/4x6yn', function (data) {
    $.each(data, function (key, val) {
      var arr = [];
      for (var i = 1961; i < 2015; i++) {
        arr.push(val[i]);
      }
      if (myJSON[key] !== undefined) {
        
        myJSON[key].DR = arr;
      }
    });
    $.getJSON('https://api.myjson.com/bins/1p8kv', function (data) {
    $.each(data, function (key, val) {
      
      if (myJSON[key] !== undefined) {
        
        myJSON[key].DBI = val[2013];
      }
    });
    $.getJSON('https://api.myjson.com/bins/28qqn', function (data) {
    $.each(data, function (key, val) {
      
      if (myJSON[key] !== undefined) {
        
        myJSON[key].DBNCD = val[2013];
      }
    });
    $.getJSON('https://api.myjson.com/bins/1rlgf', function (data) {
    $.each(data, function (key, val) {
      var arr = [];
      for (var i = 1961; i < 2015; i++) {
        if (val[i] !== null && val[i] !== '') {
          
          arr.push(Math.round(val[i]));
        }
      }
      if (myJSON[key] !== undefined) {
        
        myJSON[key].LE = arr;
      }
    });
    $.getJSON('https://api.myjson.com/bins/2jo9b', function (data) {
    $.each(data, function (key, val) {
      var arr = [];
      for (var i = 1961; i < 2015; i++) {
        if (val[i] !== null && val[i] !== '') {
          
          arr.push(Math.round(val[i]));
        }
      }
      if (myJSON[key] !== undefined) {
        myJSON[key].POP = arr;
      }
    });
    window.data = myJSON;
    localStorage.setItem('data', JSON.stringify(myJSON));
  });
});
});
});
});
});


}
}


function parseRoute(hash) {
  $('.view').hide();
  var newHash = hash.split('/')[0];
  var secondPart = hash.split('/')[1];
  switch (newHash) {
    case '':
    $('.home').show();
    break;
    case 'detail':
    $('.' + newHash).show();
    fillDetail(secondPart);
    break;
    case 'favo':
    $('.' + newHash).show();
    fillFavo();
    break;
    case 'world':
    $('.' + newHash).show();
    getLocation();
    break;
    case 'lifeextender':
    $('.' + newHash).show();
    sliderInit();
    break;
    case 'table':
    $('.' + newHash).show();
    break;
    default:
    $('.' + newHash).show();
    break;
  }
  
  $('ul.nav li').removeClass('active');
  $('ul.nav li[data-route="' + newHash + '"]').addClass('active');
}
// FILL FUNCTION
function fillTable(year) {
  $(".year").text(year);
  $.each(data, function (key, val) {
    $("#tableData").append(
    "<tr><td><a href='/#/detail/" + key + "'>" + key + "</a></td> " +
    "<td>" + val["DR"][year] + "</td>" +
    "<td>" + val["DBCD"] + "%</td>" +
    "<td>" + val["DBNCD"] + "%</td>" +
    "<td>" + val["DBI"] + "%</td>" +
    "<td class ='hidden-xs'>" + val["LE"][year] + "</td> </tr>"
    );
  });
  $('#table_id').DataTable({
    paging: false,
    info: false
  });
}
function fillFavo() {
  var favo = $('.favo');
  favo.empty();

  $.each(likes, function (key, val) {
    if (val) {
      var countryFlag = key;
      while (countryFlag.indexOf(' ') !== -1) {
        countryFlag = countryFlag.replace(' ', '-');
      }
      favo.append("<h2 class='titlefavo'><a href='./#/detail/" + key + "'>" + key + "</a><img class='flagfavo' src='https://www.countries-ofthe-world.com/flags/flag-of-" + countryFlag + ".png'></h2>");
      favo.append('<hr>');
    }
  })
    if (favo.html() == '') {
        favo.append("<h3>You don't have any favorites yet.</h3>")
    };
}
function fillDetail(country) {
  $('#detail').empty();
  var source = $('#detail-template').html();
  var template = Handlebars.compile(source);
  var polar;
  var ctx;
  var options = {
    //Boolean - Show a backdrop to the scale label
    scaleShowLabelBackdrop: true,
    
    //String - The colour of the label backdrop
    scaleBackdropColor: 'rgba(255,255,255,0.75)',
    
    // Boolean - Whether the scale should begin at zero
    scaleBeginAtZero: true,
    
    //Number - The backdrop padding above & below the label in pixels
    scaleBackdropPaddingY: 2,
    
    //Number - The backdrop padding to the side of the label in pixels
    scaleBackdropPaddingX: 2,
    
    //Boolean - Show line for each value in the scale
    scaleShowLine: true,
    
    //Boolean - Stroke a line around each segment in the chart
    segmentShowStroke: true,
    
    //String - The colour of the stroke on each segement.
    segmentStrokeColor: '#fff',
    
    //Number - The width of the stroke value in pixels
    segmentStrokeWidth: 2,
    
    //Number - Amount of animation steps
    animationSteps: 100,
    
    //String - Animation easing effect.
    animationEasing: 'easeOutBounce',
    
    //Boolean - Whether to animate the rotation of the chart
    animateRotate: true,
    
    //Boolean - Whether to animate scaling the chart from the centre
    animateScale: false,
    
    //String - A legend template
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
    
  };
  var options2 = {
    
    ///Boolean - Whether grid lines are shown across the chart
    scaleShowGridLines: true,
    
    //String - Colour of the grid lines
    scaleGridLineColor: "rgba(0,0,0,.05)",
    
    //Number - Width of the grid lines
    scaleGridLineWidth: 1,
    
    //Boolean - Whether to show horizontal lines (except X axis)
    scaleShowHorizontalLines: true,
    
    //Boolean - Whether to show vertical lines (except Y axis)
    scaleShowVerticalLines: true,
    
    //Boolean - Whether the line is curved between points
    bezierCurve: true,
    
    //Number - Tension of the bezier curve between points
    bezierCurveTension: 0.4,
    
    //Boolean - Whether to show a dot for each point
    pointDot: true,
    
    //Number - Radius of each point dot in pixels
    pointDotRadius: 4,
    
    //Number - Pixel width of point dot stroke
    pointDotStrokeWidth: 1,
    
    //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
    pointHitDetectionRadius: 20,
    
    //Boolean - Whether to show a stroke for datasets
    datasetStroke: true,
    
    //Number - Pixel width of dataset stroke
    datasetStrokeWidth: 2,
    
    //Boolean - Whether to fill the dataset with a colour
    datasetFill: true,
    
    //String - A legend template
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
    
  };
  var fav = "fa-star-o";
  
  if (likes[country]) {
    fav = "fa-star"
  }
  
  Chart.defaults.global = {
    // Boolean - Whether to animate the chart
    animation: true,
    
    // Number - Number of animation steps
    animationSteps: 60,
    
    // String - Animation easing effect
    // Possible effects are:
    // [easeInOutQuart, linear, easeOutBounce, easeInBack, easeInOutQuad,
    //  easeOutQuart, easeOutQuad, easeInOutBounce, easeOutSine, easeInOutCubic,
    //  easeInExpo, easeInOutBack, easeInCirc, easeInOutElastic, easeOutBack,
    //  easeInQuad, easeInOutExpo, easeInQuart, easeOutQuint, easeInOutCirc,
    //  easeInSine, easeOutExpo, easeOutCirc, easeOutCubic, easeInQuint,
    //  easeInElastic, easeInOutSine, easeInOutQuint, easeInBounce,
    //  easeOutElastic, easeInCubic]
    animationEasing: "easeOutQuart",
    
    // Boolean - If we should show the scale at all
    showScale: true,
    
    // Boolean - If we want to override with a hard coded scale
    scaleOverride: false,
    
    // ** Required if scaleOverride is true **
    // Number - The number of steps in a hard coded scale
    scaleSteps: null,
    // Number - The value jump in the hard coded scale
    scaleStepWidth: null,
    // Number - The scale starting value
    scaleStartValue: null,
    
    // String - Colour of the scale line
    scaleLineColor: "rgba(0,0,0,.1)",
    
    // Number - Pixel width of the scale line
    scaleLineWidth: 1,
    
    // Boolean - Whether to show labels on the scale
    scaleShowLabels: true,
    
    // Interpolated JS string - can access value
    scaleLabel: "<%=value%>",
    
    // Boolean - Whether the scale should stick to integers, not floats even if drawing space is there
    scaleIntegersOnly: true,
    
    // Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
    scaleBeginAtZero: false,
    
    // String - Scale label font declaration for the scale label
    scaleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
    
    // Number - Scale label font size in pixels
    scaleFontSize: 12,
    
    // String - Scale label font weight style
    scaleFontStyle: "normal",
    
    // String - Scale label font colour
    scaleFontColor: "#666",
    
    // Boolean - whether or not the chart should be responsive and resize when the browser does.
    responsive: false,
    
    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio: true,
    
    // Boolean - Determines whether to draw tooltips on the canvas or not
    showTooltips: true,
    
    // Function - Determines whether to execute the customTooltips function instead of drawing the built in tooltips (See [Advanced - External Tooltips](#advanced-usage-custom-tooltips))
    customTooltips: false,
    
    // Array - Array of string names to attach tooltip events
    tooltipEvents: ["mousemove", "touchstart", "touchmove"],
    
    // String - Tooltip background colour
    tooltipFillColor: "rgba(0,0,0,0.8)",
    
    // String - Tooltip label font declaration for the scale label
    tooltipFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
    
    // Number - Tooltip label font size in pixels
    tooltipFontSize: 14,
    
    // String - Tooltip font weight style
    tooltipFontStyle: "normal",
    
    // String - Tooltip label font colour
    tooltipFontColor: "#fff",
    
    // String - Tooltip title font declaration for the scale label
    tooltipTitleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
    
    // Number - Tooltip title font size in pixels
    tooltipTitleFontSize: 14,
    
    // String - Tooltip title font weight style
    tooltipTitleFontStyle: "bold",
    
    // String - Tooltip title font colour
    tooltipTitleFontColor: "#fff",
    
    // Number - pixel width of padding around tooltip text
    tooltipYPadding: 6,
    
    // Number - pixel width of padding around tooltip text
    tooltipXPadding: 6,
    
    // Number - Size of the caret on the tooltip
    tooltipCaretSize: 8,
    
    // Number - Pixel radius of the tooltip border
    tooltipCornerRadius: 6,
    
    // Number - Pixel offset from point x to tooltip edge
    tooltipXOffset: 10,
    
    // String - Template string for single tooltips
    tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %>",
    
    // String - Template string for multiple tooltips
    multiTooltipTemplate: "<%= value %>",
    
    // Function - Will fire on animation progression.
    onAnimationProgress: function () {
    },
    
    // Function - Will fire on animation completion.
    onAnimationComplete: function () {
    }
  };
  
  var context = {
    name: country,
    POP: data[country]["POP"][50],
    LE: data[country]["LE"][50],
    DR: data[country]["DR"][50],
    CH: calculateChanceToDie(country, 50),
    lat: data[country]["lat"],
    long: data[country]["lng"],
    DBI: data[country]["DBI"],
    DBCD: data[country]["DBCD"],
    DBNCD: data[country]["DBNCD"],
    fav: fav
  };
  
  var html = template(context);
  $(".view.detail").append(html);
  
  // JUST TO BE SURE
  // country = country[0].toUpperCase() + country.substr(1,country.length-1).toLowerCase();
  // YOU NEVER KNOW
  var countryFlag = country;
  while (countryFlag.indexOf(" ") !== -1) {
    countryFlag = countryFlag.replace(" ", "-");
  }
  
  var flag = $("#flag");
  var causes = $("#causes");
  flag.append('<img id="flag" class="centerIMG" src="https://www.countries-ofthe-world.com/flags/flag-of-' + countryFlag + '.png">');
  
  causes.append('<canvas id="chartCauses" class="centerIMG" width="175" height="175"></canvas>');
  
  var datachart = [
  {
    value: data[country]["DBI"],
    color: "#F7464A",
    highlight: "#FF5A5E",
    label: "Death By Injury"
  },
  {
    value: data[country]["DBCD"],
    color: "#46BFBD",
    highlight: "#5AD3D1",
    label: "Death By Communicable Disease"
  },
  {
    value: data[country]["DBNCD"],
    color: "#FDB45C",
    highlight: "#FFC870",
    label: "Death By Non Communicable Disease"
  }
  ];
  var datachart2 = {
    labels: ["2005", "2006", "2007", "2008", "2009", "2010", "2011"],
    datasets: [
    {
      label: "Population",
      fillColor: "rgba(220,220,220,0.2)",
      strokeColor: "rgba(220,220,220,1)",
      pointColor: "rgba(220,220,220,1)",
      pointStrokeColor: "#fff",
      pointHighlightFill: "#fff",
      pointHighlightStroke: "rgba(220,220,220,1)",
      //data: [data[country]["POP"][44], data[country]["POP"][45], data[country]["POP"][46], data[country]["POP"][47], data[country]["POP"][48], data[country]["POP"][49], data[country]["POP"][50]]
      data: [calculateChanceToDie(country, 44), calculateChanceToDie(country, 45), calculateChanceToDie(country, 46), calculateChanceToDie(country, 47), calculateChanceToDie(country, 48), calculateChanceToDie(country, 49), calculateChanceToDie(country, 50), calculateChanceToDie(country, 51)]
    }
    ]
  };
  
  
  ctx = document.getElementById("chartCauses").getContext("2d");
  var ctx2 = document.getElementById("chartPOP").getContext("2d");
  new Chart(ctx).Doughnut(datachart, options);
  new Chart(ctx2).Line(datachart2, options2);
  
}
//HELPER
function calculateChanceToDie(country, year) {
  return Math.round(((data[country]["DR"][year] * 1000) / (data[country]["POP"][year])) * 100000)
}
// MAP
function getLocation() {
  var progress = setInterval(function () {
    var $bar = $("#bar");
    
    if ($bar.width() >= 600) {
      clearInterval(progress);
    } else {
      $bar.width($bar.width() + 60);
    }
    $bar.text($bar.width() / 6 + "%");
    if ($bar.width() / 6 == 100) {
      $bar.text("Still working ... " + $bar.width() / 6 + "%");
    }
  }, 800);

  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
  }
}
function showPosition(position) {
  var lat = position.coords.latitude;
  var lng = position.coords.longitude;
  initMap(lat, lng);
}
function initMap(lat, lng) {
  var kmlLayer = new google.maps.KmlLayer();
  map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: lat, lng: lng},
    zoom: 3,
    minZoom: 3,
    maxZoom: 4,
    streetViewControl: false,
    mapTypeId: google.maps.MapTypeId.SATELLITE
  });
  loadKmlLayer(src, map);
  
  
}
function loadKmlLayer(src, map) {
  var kmlLayer = new google.maps.KmlLayer(src, {
    suppressInfoWindows: true,
    preserveViewport: false,
    map: map,
    zoom: 15
  });
  google.maps.event.addListener(kmlLayer, 'click', function (event) {
    var country = event.featureData.name;
    if (country in data) {
      window.location.href = "./#detail/" + country;
    }
  });
  $("#bar").width(600);
  $(".loader").fadeOut(3000);
}
// action funtions
$(document).on('click', '#favo', function () {
  var country = $("#country").text();
  var src = 'https://developers.google.com/maps/tutorials/kml/westcampus.kml';
  
  if ($("#favo").hasClass("fa-star")) {
    $("#favo").removeClass("fa-star").addClass("fa-star-o");
    likes[country] = false;
    
  }
  else {
    $("#favo").removeClass("fa-star-o").addClass("fa-star");
    likes[country] = true;
    
  }
  localStorage.setItem('favo', JSON.stringify(likes));
});
$("#distanceselect").on("slide", function (year) {
  $(".distance").text(year.value + " km");
});
$("#btnLE").click(function () {
  var value = mySlider.slider('getValue');
   if (value[0]["value"] == ""){
       km = 5000;
   }
    else
   {
       km = parseInt(value[0]["value"]);
   }
  getLocationName();
});
// EXTEND YOUR LIFE
function sliderInit() {
  // Instantiate the slider
  mySlider = $("#distanceselect").slider();
}
function getLocationName() {
  $.getJSON('http://freegeoip.net/json/', function (data) {
  countryuser = data.country_name;
});

if (navigator.geolocation) {
  navigator.geolocation.getCurrentPosition(showMyPosition);
  
}
}
function showMyPosition(position) {
  window.userLat = (position.coords.latitude);
  window.userLng = position.coords.longitude;
  
  extendYourLife();
}
function extendYourLife() {
  
  $.getJSON('https://api.myjson.com/bins/5cesv', function (dataloc) {
  
  $.each(dataloc, function (key, val) {
    window.locations[key] = {lat: val['LAT'], lng: val['LNG']}
  });
  
  
  function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2 - lat1);  // deg2rad below
    var dLon = deg2rad(lon2 - lon1);
    var a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
    Math.sin(dLon / 2) * Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return d;
  }
  
  function deg2rad(deg) {
    return deg * (Math.PI / 180)
  }
  
  $.each(data, function (country) {

    if (country in locations) {
      //console.log('The distance to ' + country + 'is ' + getDistanceFromLatLonInKm(userLat,userLng,locations[country].lat,locations[country].lng) + ' km')
      distance = getDistanceFromLatLonInKm(userLat, userLng, locations[country].lat, locations[country].lng);
      if (distance < km) {
        lstCloseEnough[country] = {distance: distance}
      }
    }
  });
  
  
  $(document).ajaxStop(function () {
    var LEuser = data[countryuser]['LE']['50'];
    var highestLE = LEuser;
    var winner = countryuser;
    
    $.each(lstCloseEnough, function (country) {
      if ((data[country]['LE']['50']) > highestLE) {
        highestLE = data[country]['LE']['50'];
        winner = country;
      }
      
    });
    difference = highestLE - LEuser;
    fillLE(winner)
    
  });
  function fillLE(winner) {
    $('.view.lifeextender').empty();
    var source = $('#LE-template').html();
    var template = Handlebars.compile(source);
    var context = {
      winner: winner,
      distance: Math.round(lstCloseEnough[winner].distance),
      year: difference
    };
    var html = template(context);
    $('.view.lifeextender').append(html);
  }
  
  
});

}
(function () {
  init();
})();











